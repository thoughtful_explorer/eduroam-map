# US Eduroam Map
Extracts US Eduroam locations from the official [Eduroam](https://incommon.org/eduroam/eduroam-u-s-locator-map/) website, and then places them into KML format.

**Note:** There are some locations without assigned coordinates that are ignored by this version, but could be geocoded in a future update.

## Dependencies
* Python 3.x
    * Requests module for getting the JSON file
    * JSON module for parsing the JSON
    * Regular Expressions for splitting strings
    * Simplekml module for easily building KML files
* Also of course depends on the [US Eduroam locations JSON file](https://fm-eduroam-public-prd.s3.amazonaws.com/institution.json) (located in an Amazon S3 bucket referenced within the official [US Eduroam locator](https://incommon.org/eduroam/eduroam-u-s-locator-map/) webpage).
