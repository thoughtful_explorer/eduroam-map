#!/usr/bin/python3

import requests
import json
import re
import simplekml

#Set filename/path for KML file output
kmlfile = "eduroam.kml"
#Set KML schema name
kmlschemaname = "eduroam"
#Set JSON URL
global jsonURL 
jsonURL = "https://fm-eduroam-public-prd.s3.amazonaws.com/institution.json"

#Example JSON response
'''
{
                "ROid": "US01",
                "address": [
                    {
                        "city": {
                            "data": "Exampletown",
                            "lang": "en"
                        },
                        "street": {
                            "data": "123 Fake St",
                            "lang": "en"
                        }
                    }
                ],
                "contact": [
                    {
                        "email": "ithelp@example.edu",
                        "name": "Information Technology Help Desk",
                        "phone": "+1 (111) 111-1111",
                        "privacy": 1,
                        "type": 0
                    }
                ],
                "coordinates": "-77.777,39.999",
                "info_URL": [
                    {
                        "data": "https://example.edu/wifi",
                        "lang": "en"
                    }
                ],
                "inst_name": [
                    {
                        "data": "Example College",
                        "lang": "en"
                    }
                ],
                "inst_realm": [
                    "example.edu",
                    "example.org"
                ],
                "inst_type": "3,3",
                "instid": "example.edu",
                "location": [
                    {
                        "AP_no": 1450,
                        "SSID": "eduroam",
                        "coordinates": "-77.777,39.999",
                        "enc_level": "WPA2+WPA",
                        "info_URL": [
                            {
                                "data": "https://example.com",
                                "lang": "en"
                            }
                        ],
                        "loc_name": [
                            {
                                "data": "Example College",
                                "lang": "en"
                            }
                        ],
                        "locationid": "FMID-XXX",
                        "stage": 1,
                        "type": 0
                    },
                    {
                        "AP_no": 60,
                        "SSID": "eduroam",
                        "coordinates": "-77.777,39.999",
                        "enc_level": "WPA2+WPA",
                        "info_URL": [
                            {
                                "data": "https://example.com",
                                "lang": "en"
                            }
                        ],
                        "loc_name": [
                            {
                                "data": "Example School Name",
                                "lang": "en"
                            }
                        ],
                        "locationid": "FMID-1343",
                        "stage": 1,
                        "type": 0
                    }
                ],
                "policy_URL": [
                    {
                        "data": "https://example.edu/wifi",
                        "lang": "en"
                    }
                ],
                "stage": 1,
                "ts": "2021-11-15T21:22:27.056Z",
                "type": "IdP+SP"
            },
'''

#Initialize kml object
kml = simplekml.Kml()
#Get the locations JSON file and JSONify it
locationsJSON = requests.get(jsonURL).json()
#Iterate through the JSON for each location (institution)
for location in locationsJSON["institutions"]["institution"]:
    #Get the name of the location
    locationname = location["inst_name"][0]["data"]
    #Try to get the coordinates of the location
    try: 
        latlng = location["coordinates"]
    #Oddly, there are some locations that don't have coordinates - I don't mind simply skipping over these for this version
    except KeyError:
        continue
    #Split the latlng with the comma
    latlng = latlng.split(',')
    #After the comma is latitude
    lat = latlng[1]
    #Before the comma is longitude
    lng = latlng[0]
    #First, create the point name and description (using the address as the description) in the kml
    point = kml.newpoint(name=locationname,description="eduroam location")
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
